﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scDownedCounter : MonoBehaviour
{
    public Text displayDowned;
    // Start is called before the first frame update
    void Start()
    {
        displayDowned = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        displayDowned.text = global.kills.ToString();
    }
}
