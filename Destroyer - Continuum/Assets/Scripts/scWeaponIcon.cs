﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scWeaponIcon : MonoBehaviour
{
    public int wpnicon = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (global.weapons[wpnicon]==true) {
            if (global.currentweapon == wpnicon)
            {
                switch (wpnicon) {
                    case 0: GetComponent<SpriteRenderer>().color = new Color(1, 0, 0, 1); break;
                    case 1: GetComponent<SpriteRenderer>().color = new Color(0, 1, 1, 1); break;
                    case 2: GetComponent<SpriteRenderer>().color = new Color(0, 1, 0, 1); break;
                    case 3: GetComponent<SpriteRenderer>().color = new Color(1, 1, 0, 1); break;
                    case 4: GetComponent<SpriteRenderer>().color = new Color(1, 0, 1, 1); break;
                }
            }
            else GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        } else GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
    }
}
