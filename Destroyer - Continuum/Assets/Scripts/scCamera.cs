﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scCamera : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Rigidbody2D>().velocity = Vector2.right * global.cameraspeed;
        global.camerapos = transform.position.x;
    }
}
